﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace FinalTermProject
{
    class mainClass
    {
        [STAThread]
        static void Main(string[] args)
        {
            
            String UserName;
            YoutubeApi objYoutubeVideo = new YoutubeApi(); 
            Console.WriteLine("YouTube UserName");
            UserName = Console.ReadLine();

            try
            {
                objYoutubeVideo.Auth().Wait();
                objYoutubeVideo.RunForVideos(UserName).Wait();
               
            }
            catch (AggregateException ex)
            {
                foreach (var e in ex.InnerExceptions)
                {
                    Console.WriteLine("Error: " + e.Message);
                }
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
