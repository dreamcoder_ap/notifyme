﻿using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using System.Data.SqlClient;


namespace FinalTermProject
{
    class YoutubeApi
    {
        private UserCredential credential;
        private YouTubeService youtubeService = new YouTubeService();

        public YouTubeService getService()
        {
            return this.youtubeService;
        }
        
        public async Task Auth()
        {
            
            using (var stream = new FileStream("C:\\Users\\Az\\documents\\visual studio 2013\\Projects\\ConsoleApplication1\\ConsoleApplication1\\js\\client_secret.json", FileMode.Open, FileAccess.Read))
            {
                this.credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,

                    new[] { YouTubeService.Scope.YoutubeReadonly },
                    "user",
                    CancellationToken.None,
                    new FileDataStore(this.GetType().ToString())
                );
            }

            this.youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = this.GetType().ToString()
            });
        }

        public async Task RunForVideos(string UserName)
        {
            DetailDB ChDetail=new DetailDB();
            int count_Video = 0;
            ChannelsResource.ListRequest channelsListRequest = youtubeService.Channels.List("contentDetails");
            channelsListRequest.ForUsername = UserName;
            ChDetail.chanelName = UserName;
            var channelsListResponse = await channelsListRequest.ExecuteAsync();



            foreach (var channel in channelsListResponse.Items)
            {

                var uploadsListId = channel.ContentDetails.RelatedPlaylists.Uploads;
                ChDetail.id = uploadsListId;

                Console.WriteLine("Videos in list {0}", uploadsListId);

                var nextPageToken = "";
                while (nextPageToken != null)
                {
                    var playlistItemsListRequest = youtubeService.PlaylistItems.List("snippet");
                    playlistItemsListRequest.PlaylistId = uploadsListId;
                    playlistItemsListRequest.MaxResults = 50;
                    playlistItemsListRequest.PageToken = nextPageToken;


                    var playlistItemsListResponse = await playlistItemsListRequest.ExecuteAsync();

            foreach (var playlistItem in playlistItemsListResponse.Items)
                    {


                        Console.WriteLine("{0} ({1})", playlistItem.Snippet.Title, playlistItem.Snippet.ResourceId.VideoId);
                        count_Video++;
                        Console.WriteLine("NO OF VID....." + count_Video);
                    }

                    nextPageToken = playlistItemsListResponse.NextPageToken;
                }

                ChDetail.NoOfVideos = count_Video;
                DbConnection objDB = new DbConnection();
                //objDB.checkForNewVideos(ChDetail, count_Video);
                Boolean exits = false;
                int newUpload;
                StreamReader reader = new StreamReader("C:\\Users\\Az\\documents\\visual studio 2013\\Projects\\ConsoleApplication1\\ConsoleApplication1\\log.txt", true);
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (line == ChDetail.id)
                    {
                        exits = true;
                        {
                            int up = 0;
                            string id = line;
                            for (int i = 0; i < 2; i++)
                            {
                                id = reader.ReadLine();
                            }
                            Console.WriteLine("IDDDDDD........... " + id);
                            up = count_Video - Convert.ToInt32(id);
                            if (up > 0)
                            {
                                Console.WriteLine(up + " New Videos ");
                            }
                        }
                    }
                }
                reader.Close();
                if (exits == false)
                {
                    DbConnection db = new DbConnection();
                    db.saveDataInFile(ChDetail);
                }
                else
                {
                    Console.WriteLine("Already there in the File");
                }
            }
        }
    }
}
